# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure(2) do |config|

  # Name of Vagrant Box
  config.vm.box = "acntech/ubuntu"

  # Don't generate new SSH key, but use the default insecure key
  config.ssh.insert_key = false

  config.vm.provider "virtualbox" do |vb|
    @filename = "personal_settings/virtualbox_settings.rb"

    # If personal settings file exists then use it
    if File.file?(@filename)
      load @filename
      
      p_settings = VirtualBoxSettings.new
      p_settings.setup_box(vb)
    else
      # Name to display in Virtualbox
      vb.name = "AcnTech Ubuntu Developer"

      # Display the VirtualBox GUI when booting the machine
      vb.gui = true

      # Customize CPU count
      vb.cpus = "1"

      # Customize the amount of memory on the VM
      vb.memory = "2048"

      # Customize amount of video memory
      vb.customize ["modifyvm", :id, "--vram", "128"]

      # Disable 3D acceleration
      vb.customize ["modifyvm", :id, "--accelerate3d", "off"]
    end
  end

  # Run provisioning
  @temp_script_path = "/tmp/bin"
  @permanent_script_path = "/usr/local/share/vagrant"
  config.vm.provision "shell", inline: "DEBIAN_FRONTEND=noninteractive mkdir -p #@temp_script_path"
  config.vm.provision "shell", inline: "DEBIAN_FRONTEND=noninteractive mkdir -p #@permanent_script_path"
  config.vm.provision "shell", inline: "DEBIAN_FRONTEND=noninteractive chown -R vagrant:vagrant #@temp_script_path"
  config.vm.provision "file", source: "../bin", destination: "#@temp_script_path"
  config.vm.provision "file", source: "./bin", destination: "#@temp_script_path"
  config.vm.provision "shell", inline: "DEBIAN_FRONTEND=noninteractive chmod -R +x #@temp_script_path"
  config.vm.provision "shell", inline: "DEBIAN_FRONTEND=noninteractive cp -r  #@temp_script_path/* #@permanent_script_path/"
  config.vm.provision "shell", inline: "DEBIAN_FRONTEND=noninteractive rm -rf #@temp_script_path"
  config.vm.provision "shell", inline: "DEBIAN_FRONTEND=noninteractive #@permanent_script_path/welcome.sh"
  config.vm.provision "shell", inline: "DEBIAN_FRONTEND=noninteractive #@permanent_script_path/provision.sh"
end
