#!/bin/bash

MODULE_VERSION="2018.3.4"
MODULE_EDITION="ideaIC" # ideaIC or ideaIU
MODULE_DESCRIPTION="IntelliJ IDEA Community" # Community or Ultimate

# These values are used to define the download URL for this module, so it is important that they are correct.