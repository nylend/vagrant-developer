#!/bin/bash

DOCKER_COMPOSE_VERSION="1.24.1"

# These values are used to define the download URL for this module, so it is important that they are correct.