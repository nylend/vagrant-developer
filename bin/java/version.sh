#!/bin/bash

MODULE_VERSION="1.8.0_221"
MODULE_ARCHIVE_DOWNLOAD_URL="https://download.oracle.com/otn/java/jdk/8u221-b11/230deb18db3e4014bb8e3e8324f81b43/jdk-8u221-linux-x64.tar.gz"

# These values are used to define the download URL for this module, so it is important that they are correct.